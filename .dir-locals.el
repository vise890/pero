;; https://docs.cider.mx/cider/basics/up_and_running.html#clojure-cli-options
((clojure-mode
  (cider-clojure-cli-aliases . ":dev:test")))
