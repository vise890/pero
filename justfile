alias cljfmt := fmt
alias antq := upgrade-deps

# run tests
test:
  clj -M:test

# reformat codebase
fmt:
  clj -M:cljfmt
  prettier --write README.md

# upgrade project dependencies
upgrade-deps:
  clj -T:antq

# lint the codebase
lint:
  clj -M:eastwood
  clj-kondo --lint src test

# run all tasks (e.g., in preparation for a commit)
all: upgrade-deps test fmt lint
