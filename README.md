# 🍐 pero

A clone of [aero](https://github.com/juxt/aero), but with `#include/fragment`s able to refer to top-level `#ref`s.

## Usage

```clj
(require '[pero.core :as pero])
(pero/load {:profile :foo} "data.edn")
```

```clj
;; data.edn
{;; Pero!supports standard edn values:
 :n   1
 :set #{1 2 3}
 :map {:a [0 1 2]}
 ;; ...

 ;; Pero augments EDN by introducing some useful tag readers (mostly equal to Aero's)

 ;; you can do lightweight string processing:
 "abc"     #str/join ["a" "b" "c"]
 "a"       #str/trim "   a \t"
 "hello!!" #str/format ["hello%s%s" "!" "!"]

 ;; you can parse values:
 true #parse/boolean "true"
 42   #parse/long "42"
 0.42 #parse/double "0.42"

 ;; you can cast values to keywords
 :kw #keyword "kw"

 ;; you can merge maps
 :merged #merge [{:a 1} {:b 2}]

 ;; you can use #or to take the first truthy value
 :or #or [false :xyz]

 ;; values can refer to other values:
 :0 #ref [:map :a 0]

 ;; refs can refer to other refs:
 :0' #ref [:0]

 ;; you can get values from the environment
 :user #sys/env "USER"

 ;; `#opts/get` gets arbitrary values from the `opts` passed to `pero.core/load`
 :x #opts/get :foo

 ;; `#opts/profile` gets the value associated to `(:profile opts)` passed to `pero.core/load`
 :p #opts/profile {:foo 42, :bar 43}
 ;; .. for example, if you invoked `(pero.core/load {:profile :foo} "showcase.edn")`
 ;;    the above would evaluate to `42`

 ;; tags can be mixed arbitrarily:
 :1 #ref [:map #keyword #str/trim " a " #ref [:0]]

 ;; Tags in Clojure's `default-data-readers` and `*data-readers*` can also be used
 :uuid #uuid "814c9f04-0f31-404f-9738-191adb42fcd5"

 ;; You can add your own tag-readers by adding methods to `pero.core/tag-reader`
 ;; Let's say you have this implementation:
 ;;   - `opts` is the arg passed to `pero.core/load`
 ;;   - `root` is the unevaluated edn where all tag literals are wrapped in
 ;;     `{:tag tag, :v v}`
 ;;   - `tag` is the tag name (`str` in the example)
 ;;   - `v` is the tagged value (`123` in the example)
 ;; (defmethod sut/tag-reader 'str
 ;;   [opts root tag v]
 ;;   (str v))
 "123" #str 123

 ;; You can include other configs with `#include/fragment` and `#include`

 ;; `#include` reads the config as if it was loaded with `pero.core/load`
 :include #include #io/resource "x.edn"

 ;; on the other hand, `#ref`s in the `#include/fragment`s will refer to the root config
 :frag #include/fragment #io/resource "x.edn"

 ,,,}
```

## Available tags

- `#include/fragment`
- `#include`
- `#io/file`
- `#io/resource`
- `#keyword`
- `#merge`
- `#opts/get`
- `#opts/get-in`
- `#opts/profile`
- `#or`
- `#parse/boolean`
- `#parse/double`
- `#parse/long`
- `#ref`
- `#str/format`
- `#str/join`
- `#str/trim`
- `#sys/env`
- `#sys/envf`
- .. and any other readers defined in `clojure.core/default-data-readers` and `clojure.core/*data-readers*`

## Major differences with Aero

- In `#include/fragment`s, `#ref`s refer to root.
  This [isn't/won't be supported in aero](https://github.com/juxt/aero/issues/92);
- Everything is evaluated (**possibly multiple times**);
- no resolvers: use `#io/resource`, `#io/file` or roll your own.

## Motivation

I want a data templating language more than a config lang. what I want to
achieve is templating of `yaml`/`json` (like ECS task definitions/K8s manifests):

```clj
;; ecs-task-definition.edn
{:app/name "my-app"
 :app/image #str/format ["123.dkr.ecr.eu-west-1.amazonaws.com/my-org/%s:%s" #ref [:app/name] #sys/env GIT_SHA]
 :app/portMappings [{:containerPort 8080, :protocol "tcp"}]
 :containerDefinitions [;; NOTE: these can refer to top the :app/* keys above
                        #include/fragment #io/resource "xyz/ecs/containerDefinitions/app.edn"
                        #merge [{:cpu 128 :mem 512}
                                #include/fragment #io/resource "xyz/ecs/containerDefinitions/aws-otel-collector.edn"]]}
```

## Not supported

- Lazy evaluation when `#ref`fing
  - it would be nice to be able to `#ref` to `[:b :c]`, even though `[:b :a]` depends on `[:a]`:
    ```clj
    {:a [#ref [:b :c]]
     :b {:a #ref [:a]
         :c 42}}
    ```

## Credits

- [aero](https://github.com/juxt/aero)
