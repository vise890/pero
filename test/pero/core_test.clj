(ns pero.core-test
  (:require
   [clojure.java.io :as io]
   [clojure.test :as t :refer [deftest is testing]]
   [medley.core :as m]
   [pero.core :as sut]))

(defn load-str
  ([opts s]
   (->> s java.io.StringReader. (sut/load opts)))
  ([s]
   (load-str {} s)))

(deftest load-test

  (testing "just plain values"
    (is (= {:a 42 :b #{1} :c [] :d {}}
           (load-str "{:a 42 :b #{1} :c [] :d {}}"))))

  (testing "sets"
    (is (= {:a #{1 :a 42} :b 42}
           (load-str
            "{:a #{1 #or [nil :a] #ref [:b]} :b 42}"))))

  (testing "vectors"
    (is (= {:a [1 :a 42] :b 42}
           (load-str
            "{:a [1 #or [nil :a] #ref [:b]] :b 42}"))))

  (testing "sequentials"
    (is (= {:a [1 :a 42] :b 42}
           (load-str
            "{:a (1 #or [nil :a] #ref [:b]) :b 42}"))))

  (testing "evalling map keys"
    (is (= {#{:foo} 1}
           (load-str "{#{#keyword #str/trim #or [false \"  foo \"]} 1}")))))

(deftest tag-reader-test

  (testing "#ref"
    (is (= {:a 42 :b 42}
           (load-str
            "{:a 42 :b #ref [:a]}")))

    (testing "referring to other `#ref`s"
      (is (= {:a :a, :b :a}
             (load-str "{:a :a, :b #ref [#ref [:a]]}")))
      (is (= {:a :a, :b [:a :b], :c [:a :b]}
             (load-str "{:a :a
                         :b [#ref [:a] :b]
                         :c #ref [:b]}"))))

    (testing "with tagged vals in ref path"
      (is (= {:foo {:x 1, :y 2}
              :foo' 1}
             (load-str "{:foo {:x 1, :y 2}
                         :foo' #ref [:foo #keyword #str/trim \"  x\"]}")))
      (testing "with refs in ref path"
        (is (= {:a {:b 42}
                :b :b
                :c 42}
               (load-str "{:a {:b 42}
                           :b :b
                           :c #ref [:a #ref [:b]]}")))))

    (testing "with tagged vals downstream"
      (is (= {:a 42 :b 42}
             (load-str
              "{:a #or [nil 42] :b #ref [:a]}")))
      (is (= {:a {:b ["foobar"]} :x "foobar"}
             (load-str "{:a {:b [#str/join [\"foo\" \"bar\"]]}
                         :x #ref [:a :b 0]}"))))

    (testing "with tagged vals upstream"
      (is (= {:a {:b :b} :b :b}
             (load-str
              "{:a #merge [{:b :b} {}] :b #ref [:a :b]}")))))

  (testing "#or"
    (is (= {:a 42}
           (load-str
            "{:a #or [nil 42]}")))
    #_(testing "FIXME #or followed by other tag"
        (is (= {:a 42}
               (load-str {:xs [nil 42]}
                         "{:a #or #opts/get :xs}"))))
    (testing "default val"
      (is (= nil (load-str "#or [false false]"))))
    (testing "nested or"
      (is (= {:a 1}
             (load-str
              "{:a #or [#or [nil false] 1]}"))))
    (testing "nested tagged vals"
      (is (= {:a 1}
             (load-str
              "{:a #or [#sys/env NONEXISTENT 1 #ref [:non :existent] 1]}")))))

  (testing "#sys/*"
    (testing "#sys/env"
      (testing "with symbol as a name"
        (is (= (System/getenv "USER")
               (load-str "#sys/env USER"))))
      (testing "with string as a name"
        (is (= (System/getenv "USER")
               (load-str "#sys/env \"USER\" ")))))

    (testing "#sys/envf"
      (testing "with symbol as a name"
        (is (= (str "foo" (System/getenv "USER") "bar")
               (load-str "#sys/envf [\"foo%sbar\" USER]"))))
      (testing "with string as a name"
        (is (= (str "foo" (System/getenv "USER") "bar")
               (load-str "#sys/envf [\"foo%sbar\" \"USER\"]"))))))

  (testing "#str/*"
    (is (= "foo42bar" (load-str "#str/join [\"foo\" 42 \"bar\"]")))
    (is (= "foo" (load-str "#str/trim \"  foo \t\n \"")))
    (is (= "foo42" (load-str "#str/format [\"foo%s\" 42]")))
    (is (= "foo42bar" (load-str "#str/join [#str/format [\"foo%s\" 42] \"bar\"]"))))

  (testing "#parse/*"
    (testing "#parse/boolean"
      (is (= true (load-str "#parse/boolean \"true\"")))
      (is (= true (load-str "#parse/boolean \"True\"")))
      (is (= false (load-str "#parse/boolean \"false\"")))
      (is (= nil (load-str "#parse/boolean \"\"")))
      (is (= nil (load-str "#parse/boolean nil")))
      (is (= nil (load-str "#parse/boolean \"FOO\""))))
    (is (= 0.42 (load-str "#parse/double \"0.42\"")))
    (is (= 42 (load-str "#parse/long \"42\""))))

  (testing "#keyword"
    (is (= :foo (load-str "#keyword foo")))
    (is (= :foo (load-str "#keyword :foo"))))

  (testing "#merge"
    (is (= {:a 1 :b 2} (load-str "#merge [{:a 1} {:b 2} {}]"))))

  (testing "#opts/*"

    (testing "#opts/get"
      (is (= 42 (load-str {:foo 42} "#opts/get :foo"))))

    (testing "#opts/get-in"
      (is (= 42 (load-str {:foo {:bar 42}} "#opts/get-in [:foo :bar]"))))

    (testing "#opts/profile"
      (is (= 42
             (load-str {:profile :foo}
                       "#opts/profile {:foo 42 :bar 43}")))
      (testing "nonexistent profile"
        (is (= nil
               (load-str {:profile :bar}
                         "#opts/profile {:foo 1}"))))))

  (testing "#include/* & #io/*"

    (testing "#io/resource"
      (is (= (io/resource "x")
             (load-str "#io/resource \"x\""))))
    (testing "#io/file"
      (is (= (io/file "x")
             (load-str "#io/file [\"x\"]"))))

    (testing "#include"
      (testing "#opts/get"
        (is (= {:a :a, :ref/a :a, :opts/foo :foo}
               (load-str {:foo :foo} "#include #io/resource \"conf.fragment.edn\""))))
      (testing "reffing to keys in included resource"
        (is (= {:a :a, :ref/a :a, :opts/foo nil}
               (load-str "#include #io/resource \"conf.fragment.edn\"")))))

    (testing "#include/fragment"
      (testing "#io/resource"
        (is (= {:x 42}
               (load-str "#include/fragment #io/resource \"x.edn\""))))
      (testing "#io/file"
        (is (= {:x 42}
               (load-str "#include/fragment #io/file [\"test\" \"resources\" \"x.edn\"]"))))
      (testing "#opts/get"
        (is (= {:a 42, :fragment {:a :a, :ref/a 42, :opts/foo :foo}}
               (load-str {:foo :foo}
                         "{:a 42
                           :fragment #include/fragment #io/resource \"conf.fragment.edn\"}"))))
      (testing "reffing to keys in root"
        (is (= {:a 42, :fragment {:a :a, :ref/a 42, :opts/foo nil}}
               (load-str "{:a 42
                           :fragment #include/fragment #io/resource \"conf.fragment.edn\"}"))))
      (testing "evalling include paths"
        (is (= {:a             1
                :fragment/path "conf.fragment.edn"
                :fragment/v    {:a :a, :ref/a 1, :opts/foo nil}}
               (load-str "{:a 1
                           :fragment/path #str/join [\"conf\" \".fragment\" \".edn\"]
                           :fragment/v #include/fragment #io/resource #ref [:fragment/path]}"))))))

  (testing "fallbacks"

    (testing "readers in `default-data-readers`"
      (let [uuid (random-uuid)]
        (is (= uuid
               (load-str (format "#uuid \"%s\" " uuid))))
        (testing "with other tag-readers"
          (is (= {:uuid-str (str uuid)
                  :uuid uuid}
                 (load-str (format "{:uuid-str \"%s\" :uuid #uuid #ref [:uuid-str]}" uuid)))))))

    (testing "readers in `*data-readers`"
      (binding [*data-readers* {'inc inc}]
        (is (= 2
               (load-str "#inc 1")))
        (testing "with other tag-readers"
          (is (= 2
                 (load-str "#inc #parse/long \"1\"")))))
      (testing "`*data-readers*` take priority over `default-data-readers`"
        (binding [*data-readers* {'uuid (constantly "UUID")}]
          (is (= "UUID"
                 (load-str "#uuid 1")))))))

  (testing "custom `tag-reader` impls"

    (defmethod sut/tag-reader 'str
      [_opts _root _tag v]
      (str v))

    (is (= "1" (load-str "#str 1")))))

(deftest short-circuit-test

  (defmethod sut/tag-reader 'boom!
    [_opts _root _tag _v]
    (throw (ex-info "Should not have been called!!" {})))

  (testing "#or"
    (is (= :ok
           (load-str "#or [:ok #boom! 1]"))))

  (testing "#opts/profile"
    (is (= :ok
           (load-str {:profile :foo}
                     "#opts/profile {:foo :ok, :no #boom! 1}")))))

(deftest showcase-test
  (defmethod sut/tag-reader 'str
    [_opts _root _tag v]
    (str v))

  (testing "all vals evaluate to something"
    (is (empty? (m/filter-vals nil?
                               (sut/load {:profile :foo, :foo :foo} (io/resource "showcase.edn")))))))

(declare thrown-with-msg+data?)
(defmethod t/assert-expr 'thrown-with-msg+data? [msg form]
  (let [ex-msg-re (nth form 1)
        ex-data   (nth form 2)
        body      (nthnext form 2)]
    `(try
       ~@body
       (t/do-report
        {:type     :fail,  :message ~msg
         :expected '~form, :actual  ~nil})
       (catch clojure.lang.ExceptionInfo ex#
         (let [expected# ~ex-msg-re
               actual#   (.getMessage ex#)]
           (t/do-report {:type     (if (re-matches expected# actual#) :pass :fail)
                         :message  ~msg
                         :expected expected#, :actual actual#}))
         (let [expected# ~ex-data
               actual#   (ex-data ex#)]
           (t/do-report {:type     (if (= expected# actual#) :pass :fail)
                         :message  ~msg
                         :expected expected#, :actual actual#}))))))

(deftest errors-test
  (testing "no reader function test"
    (is (thrown-with-msg+data? #"No tag-reader implementation for tag foo"
                               {:type :pero.error/no-reader-fn
                                :tag 'foo
                                :val 1}
                               (load-str "#foo 1"))))

  (testing "circular `#ref`s"
    (is (thrown-with-msg+data? #"Cyclic `#ref`s detected!"
                               {:type :pero.error/ref-cycles
                                :b    {:msg           ":b depends on (:a), but (:a) also depends on :b",
                                       :circular-deps '(:a)},
                                :a    {:msg           ":a depends on (:b), but (:b) also depends on :a",
                                       :circular-deps '(:b)}}
                               (load-str "{:a #ref [:b] :b #ref [:a]}")))

    (testing "refs in ref path"
      ;; FIXME: cannot determine which cycles were present in this case
      (is (thrown-with-msg+data? #"Cyclic `#ref`s detected!"
                                 {:type :pero.error/ref-cycles}
                                 (load-str "{:a #ref [#ref [:b]] :b #ref [:a]}")))
      (is (thrown-with-msg+data? #"Cyclic `#ref`s detected!"
                                 {:type :pero.error/ref-cycles
                                  :b    {:msg           ":b depends on (:a), but (:a) also depends on :b",
                                         :circular-deps '(:a)},
                                  :a    {:msg           ":a depends on (:b), but (:b) also depends on :a",
                                         :circular-deps '(:b)}}
                                 (load-str "{:a/path :a, :a #ref [:b], :b #ref [#ref [:a/path]]}"))))

    (testing "reffing to self"
      (is (thrown-with-msg+data? #"Cyclic `#ref`s detected!"
                                 {:type :pero.error/ref-cycles,
                                  :a {:msg ":a depends on (:a), but (:a) also depends on :a",
                                      :circular-deps '(:a)}}
                                 (load-str "{:a {:a #ref [:a :a]}}")))
      (is (thrown-with-msg+data? #"Cyclic `#ref`s detected!"
                                 {:type :pero.error/ref-cycles,
                                  :a {:msg ":a depends on (:a), but (:a) also depends on :a",
                                      :circular-deps '(:a)}}
                                 (load-str "{:a {:a 1, :b #ref [:a :a]}}"))))

    (testing "multiple circular deps"
      (is (thrown-with-msg+data? #"Cyclic `#ref`s detected!"
                                 {:type :pero.error/ref-cycles
                                  :a    {:msg           ":a depends on (:b :c), but (:b :c) also depend on :a",
                                         :circular-deps '(:b :c)},
                                  :b    {:msg           ":b depends on (:a), but (:a) also depends on :b",
                                         :circular-deps '(:a)},
                                  :c    {:msg           ":c depends on (:a), but (:a) also depends on :c",
                                         :circular-deps '(:a)}}
                                 (load-str "{:a [#ref [:b] #ref [:c]]
                                             :b #ref [:a]
                                             :c #ref [:a]}"))))

    (testing "multiple-element #ref path"
      (is (thrown-with-msg+data? #"Cyclic `#ref`s detected!"
                                 {:type :pero.error/ref-cycles
                                  :a    {:msg           ":a depends on (:b), but (:b) also depends on :a",
                                         :circular-deps '(:b)},
                                  :b    {:msg           ":b depends on (:a), but (:a) also depends on :b",
                                         :circular-deps '(:a)}}
                                 ;; FIXME: this would be fine if we were lazy, but it isn't as get-in* is eager
                                 ;;        so it'll try to eval the whole of [:b] when resolving `[:b :c]`
                                 (load-str "{:a [#ref [:b :c]]
                                             :b {:a #ref [:a]
                                                 :c 42}}")))

      (is (thrown-with-msg+data? #"Cyclic `#ref`s detected!"
                                 {:type :pero.error/ref-cycles
                                  :a    {:msg           ":a depends on (:b :d), but (:b :d) also depend on :a",
                                         :circular-deps '(:b :d)},
                                  :b    {:msg           ":b depends on (:a), but (:a) also depends on :b",
                                         :circular-deps '(:a)},
                                  :d    {:msg           ":d depends on (:a), but (:a) also depends on :d",
                                         :circular-deps '(:a)}}
                                 (load-str "{:a [#ref [:b :c], #ref [:d]]
                                             :b {:a #ref [:a]
                                                 :c 42}
                                             :d #ref [:a]}"))))))
