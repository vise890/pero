(ns pero.core
  (:refer-clojure :exclude [eval load])
  (:require
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [clojure.string :as str]
   [clojure.walk :as walk]
   [malli.core :as malli]
   [medley.core :as m]))

(defn ^:private ->tagged-val [t x]
  ^{:type ::tagged-val} {:tag t :v x})
(defn ^:private tagged-val? [x]
  (= ::tagged-val (type x)))

(defn ^:private read-edn
  "Reads `r` into edn that can be passed to `eval`"
  [r]
  (with-open [r (java.io.PushbackReader. (io/reader r))]
    (->> r
         (edn/read {;; HACK: prevent edn/read from using any other data-reader
                    ;;      (like default-data-readers or *data-readers*).
                    ;;      The default tag-reader implementation will look them
                    ;;      up later.
                    ;;      This is to allow for applying data-readers *after*
                    ;;      pero's readers (e.g., "tagged-val}))))#uuid #env MY_UUID")
                    :readers (reify
                               clojure.lang.ILookup
                               (valAt [_ t] (partial ->tagged-val t)))
                    :default #(throw (Exception. "default data reader accessed! check note in source code."))}))))

(declare eval*)
(declare eval)

(defn ^:private third [xs] (nth xs 2))
(def ^:private tag-reader-args
  [:and
   [:catn [:opts :any] [:root :any] [:tag :symbol] [:v :any]]
   [:multi {:dispatch third
            :registry {::args- [:cat :any :any :any]
                       ::named [:or :string :keyword :symbol]
                       :io/factory [:fn (partial satisfies? io/IOFactory)]}}
    ['or [:cat ::args- [:sequential :any]]]
    ['opts/profile [:cat ::args- :map]]
    ['ref [:cat ::args- [:sequential :any]]]
    ['sys/env [:cat ::args- ::named]]
    ['sys/envf [:cat ::args- [:schema [:catn [:fmt :string] [:vars [:* ::named]]]]]]
    ['str/join [:cat ::args- [:sequential :any]]]
    ['str/trim [:cat ::args- :string]]
    ['str/format [:cat ::args- [:schema [:catn [:fmt :string] [:args [:* :any]]]]]]
    ['parse/boolean [:cat ::args- [:or :nil :string]]]
    ['parse/double [:cat ::args- :string]]
    ['parse/long [:cat ::args- :string]]
    ['keyword [:cat ::args- ::named]]
    ['merge [:cat ::args- [:sequential [:or :map :nil]]]]
    ['opts/get [:cat ::args- :any]]
    ['opts/get-in [:cat ::args- [:sequential :any]]]
    ['io/resource [:cat ::args- :string]]
    ['io/file [:cat ::args- [:sequential :any]]]
    ['include/fragment [:cat ::args- :io/factory]]
    ['include [:cat ::args- :io/factory]]
    [::malli/default [:cat ::args- :any]]]])
(def ^:private validate-tag-reader-args!
  (malli/coercer tag-reader-args))
(defn ^:private tag-reader-dispatch
  "Dispatches on tag & does arg validation for built-in tag-readers."
  [& [_opts _root tag _v :as args]]
  (validate-tag-reader-args! args)
  tag)

(defmulti ^:private tag-reader+
  "Evals a tagged value.

  Must control evaluation (e.g., to implement `#or` or `#profile`).
  `_v` will be passed *unevaluated*."
  tag-reader-dispatch)
(defmethod tag-reader+ 'or
  [opts root _tag xs]
  (some #(eval* opts root %)
        xs))
(defmethod tag-reader+ 'opts/profile
  [opts root _tag m]
  (eval* opts root (get m (:profile opts))))

(defmulti tag-reader
  "Evals a tagged value. `_v` will be passed already *evaluated*."
  tag-reader-dispatch)
(defmethod tag-reader 'ref
  [opts root _tag ks]
  ;; NOTE: ks is the path this is reffing to
  ;; NOTE: the eval is going to be recomputed later,
  ;;       but i wanna see if i can get away with this naive implementation first
  (let [m (eval* opts root (get root (first ks)))]
    (get-in m (rest ks))))
(defmethod tag-reader 'sys/env
  [_opts _root _tag n]
  (System/getenv (str n)))
(defmethod tag-reader 'sys/envf
  [_opts _root _tag [format-str & names]]
  (apply format
         format-str
         (map #(System/getenv (str %))
              names)))
(defmethod tag-reader 'str/join
  [_opts _root _tag ss]
  (str/join ss))
(defmethod tag-reader 'str/trim
  [_opts _root _tag s]
  (str/trim s))
(defmethod tag-reader 'str/format
  [_opts _root _tag args]
  (apply format args))
(defmethod tag-reader 'parse/boolean
  [_opts _root _tag s]
  (some-> s str/lower-case parse-boolean))
(defmethod tag-reader 'parse/double
  [_opts _root _tag s]
  (parse-double s))
(defmethod tag-reader 'parse/long
  [_opts _root _tag s]
  (parse-long s))
(defmethod tag-reader 'keyword
  [_opts _root _tag s]
  (keyword s))
(defmethod tag-reader 'merge
  [_opts _root _tag ms]
  (apply merge ms))
(defmethod tag-reader 'opts/get
  [opts _root _tag k]
  (get opts k))
(defmethod tag-reader 'opts/get-in
  [opts _root _tag ks]
  (get-in opts ks))
(defmethod tag-reader 'io/resource
  [_opts _root _tag n]
  (io/resource n))
(defmethod tag-reader 'io/file
  [_opts _root _tag args]
  (apply io/file args))
(defmethod tag-reader 'include/fragment
  [opts root _tag n]
  (eval* opts root (read-edn n)))
(defmethod tag-reader 'include
  [opts _root _tag n]
  (eval opts (read-edn n)))
(defmethod tag-reader :default
  [_opts _root tag v]
  (if-let [reader (get (merge default-data-readers *data-readers*) tag)]
    (reader v)
    (throw (ex-info (str "No tag-reader implementation for tag " tag)
                    {:type :pero.error/no-reader-fn
                     :tag tag
                     :val v}))))

(defn ^:private eval-tagged-val
  [opts root tag v]
  (if (get-method tag-reader+ tag)
    (tag-reader+ opts root tag v)
    (tag-reader opts root tag (eval* opts root v))))

(defn ^:private eval*
  [opts root edn]
  (let [eval* (partial eval* opts root)]
    (cond
      (vector? edn)      (mapv eval* edn)
      (set? edn)         (into #{} (map eval*) edn)
      (sequential? edn)  (map eval* edn)
      (tagged-val? edn)  (eval-tagged-val opts root (:tag edn) (:v edn))
      (associative? edn) (m/map-kv #(mapv eval* %&) edn)
      :else              edn)))

(defn ^:private referred-keys
  "Collects all the keys (first elements of paths) of `#ref`s"
  [opts root edn]
  (let [rs (volatile! #{})]
    (walk/postwalk #(do (when (and (tagged-val? %) (= 'ref (:tag %)))
                          (vswap! rs conj (eval* opts root (first (:v %)))))
                        %)
                   edn)
    (sort @rs)))
(defn ^:private ref-dependencies
  [opts root edn]
  (->> edn
       (referred-keys opts root)
       (into (sorted-map)
             (map (fn [rp] [rp (referred-keys opts root (get edn rp))])))))
(defn ^:private assert-no-circular-refs!
  "Tries to assert that there are no circular `#ref`s."
  [opts root edn]
  (try
    (let [deps (ref-dependencies opts root edn)
          circular-deps (seq (for [[rk rk-deps] deps
                                   :let [circular-rk-deps (seq (filter #(contains? (set (get deps %)) rk)
                                                                       rk-deps))
                                         one? (= 1 (count circular-rk-deps))]
                                   :when circular-rk-deps]
                               [rk {:msg (str rk " depends on " circular-rk-deps ", "
                                              "but " circular-rk-deps " "
                                              "also depend" (when one? "s") " on " rk)
                                    :circular-deps circular-rk-deps}]))]
      (when circular-deps
        (throw (ex-info "Cyclic `#ref`s detected!"
                        (into {:type :pero.error/ref-cycles}
                              circular-deps)))))
    (catch StackOverflowError _
      ;; FIXME: Since `#ref` paths can refer to other refs and contain tag
      ;;        literals, some lightweight evalling needs to be done, which may
      ;;        not terminate.
      (throw (ex-info "Cyclic `#ref`s detected!" {:type :pero.error/ref-cycles})))))

(defn ^:private eval
  "Fully evals `edn` read by `read-edn`"
  ([edn]
   (eval {} edn))
  ([opts edn]
   (assert-no-circular-refs! opts edn edn)
   (eval* opts edn edn)))

(defn load
  "Reads edn from `r`, evals tagged values.

  See README.md for more usage info."
  ([r]
   (load {} r))
  ([opts r]
   (->> r read-edn (eval opts))))
